/* ============
 * Mutation types for the account module
 * ============
 *
 * The mutation types that are available
 * on the auth module.
 */

export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const CHECK = 'CHECK'

export default {
  LOGIN,
  LOGOUT,
  CHECK
}
