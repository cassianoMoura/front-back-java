/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import {
  LOGIN,
  LOGOUT,
  CHECK
} from './mutation-types'

export default {
  [CHECK] (state) {
    state.token = localStorage.getItem('token')
  },
  [LOGIN] (state, token) {
    state.token = token
    localStorage.setItem('token', token)
  },
  [LOGOUT] (state) {
    state.token = ''
    localStorage.clear()
  }
}
