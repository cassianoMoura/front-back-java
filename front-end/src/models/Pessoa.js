import moment from 'moment'

export default class Pessoa {
  constructor (id, nome, sexo, endereco, cidade, estado, documento, dtNascimento, active) {
    this._id = id || null
    this._nome = nome || null
    this._sexo = sexo || null
    this._endereco = endereco || null
    this._cidade = cidade || null
    this._estado = estado || null
    this._documento = documento || null
    this._dtNascimento = dtNascimento || null
    this._active = active || true
  }

  montaModel (pessoa) {
    this._id = pessoa.id
    this._nome = pessoa.nome
    this._sexo = pessoa.sexo
    this._endereco = pessoa.endereco
    this._cidade = pessoa.cidade
    this._estado = pessoa.estado
    this._documento = pessoa.documento
    this._dtNascimento = pessoa.dtNascimento
    this._active = pessoa.active
  }

  set id (id) {
    this._id = id
  }

  get id () {
    return this._id
  }

  set nome (nome) {
    this._nome = nome
  }

  get nome () {
    return this._nome
  }

  set sexo (sexo) {
    this._sexo = sexo
  }

  get sexo () {
    return this._sexo
  }

  set endereco (endereco) {
    this._endereco = endereco
  }

  get endereco () {
    return this._endereco
  }

  set documento (documento) {
    this._documento = documento
  }

  get documento () {
    return this._documento
  }

  set dtNascimento (dtNascimento) {
    this._dtNascimento = dtNascimento
  }

  get dtNascimento () {
    return this._dtNascimento
  }

  set active (active) {
    this._active = active
  }

  get active () {
    return this._active
  }

  set cidade (cidade) {
    this._cidade = cidade
  }

  get cidade () {
    return this._cidade
  }

  set estado (estado) {
    this._estado = estado
  }

  get estado () {
    return this._estado
  }

  get json () {
    return {
      id: this._id,
      nome: this._nome,
      sexo: this._sexo,
      endereco: this._endereco,
      documento: this._documento,
      cidade: this._cidade,
      estado: this._estado,
      dtNascimento: moment(this._dtNascimento).format('YYYY-MM-DD hh:mm:ss'),
      active: this._active
    }
  }
}
