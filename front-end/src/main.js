// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/pt-br'
import Vuelidate from 'vuelidate'
import VueCharts from 'vue-charts'

Vue.use(Vuelidate)
Vue.use(VueCharts)

Vue.config.productionTip = false

Vue.use(ElementUI, { locale })

require('@/assets/main.scss')

store.dispatch('auth/check')

Vue.filter('mostraTelefone', fone => {
  if (fone) {
    let mascara = '('
    if (fone.length === 10) {
      for (let i = 0; i < fone.length; i++) {
        if (i === 1) {
          mascara += fone.charAt(i) + ') '
        } else if (i === 5) {
          mascara += fone.charAt(i) + '-'
        } else {
          mascara += fone.charAt(i)
        }
      }
      return mascara
    } else if (fone.length === 11) {
      for (let i = 0; i < fone.length; i++) {
        if (i === 1) {
          mascara += fone.charAt(i) + ') '
        } else if (i === 6) {
          mascara += fone.charAt(i) + '-'
        } else {
          mascara += fone.charAt(i)
        }
      }
      return mascara
    } else {
      return 'Formato inválido'
    }
  } else {
    return 'Não cadastrado'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
