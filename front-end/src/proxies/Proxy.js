import axios from 'axios'

axios.defaults.baseURL = process.env.BASE_URL
export default class BaseProxy {
  constructor (endpoint) {
    this.endpoint = endpoint
  }

  findAll () {
    return new Promise((resolve, reject) => {
      axios.get(`${this.endpoint}`, this.headers)
        .then((response) => resolve(response.data))
        .catch((erro) => reject(erro.data) || reject(erro))
    })
  }

  findOneById (id) {
    return new Promise((resolve, reject) => {
      axios.get(`${this.endpoint}/${id}`, this.headers)
        .then((response) => resolve(response.data))
        .catch((erro) => reject(erro.data) || reject(erro))
    })
  }

  create (item) {
    return new Promise((resolve, reject) => {
      axios.post(`${this.endpoint}`, item, this.headers)
        .then((response) => resolve(response.data))
        .catch((erro) => reject(erro.data) || reject(erro))
    })
  }

  update (id, item) {
    return new Promise((resolve, reject) => {
      axios.post(`${this.endpoint}/${id}`, item)
        .then((response) => resolve(response.data))
        .catch((erro) => reject(erro.data) || reject(erro))
    })
  }

  disable (id) {
    return new Promise((resolve, reject) => {
      axios.post(`${this.endpoint}/desativar/${id}`)
        .then((response) => resolve(response.data))
        .catch((erro) => reject(erro.data) || reject(erro))
    })
  }
}
