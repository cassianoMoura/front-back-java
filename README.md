# NEPPO - Teste Front + Back Java

#Projeto a ser desenvolvido:

Desenvolver projeto web based utilizando as tecnologias HTML5, Javascript, CSS3 e Spring Boot.
Funcionalidades que deverão ser desenvolvidas:

	CRUD de pessoas
		Criar tela de pesquisa, cadastro, edição e exclusão de pessoas.
			Objeto "Pessoa" terá os seguintes atributos: nome, data nascimento, documento de identificação, sexo, endereco.
			Incluir validação de obrigatório para todos os campos: documento de identificação, nome, data nascimento e sexo.
			
	Relatorio de pessoas
		Criar tela que contenha resultados graficos das pessoas.
			Grafico da faixa de idades das pessoas, faixas: ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"]
			Grafico contendo quantidade de pessoas que tenham sexo masculino e feminino
	
	Criar serviços RESTful para comunicação entre o back e o front

#Pré requisitos, tecnologias:

	Javascript:
		- Criar projeto modularizado, aplicar conceitos ECMAScript 6 modules, AMD ou CommonJS(http://2ality.com/2014/09/es6-modules-final.html)

	HTML e CSS:
		- Utilizar HTML5
		- Utilizar CSS3(https://developer.mozilla.org/pt-BR/docs/Web/CSS/CSS3) & SASS(se preciso)
		- Desenvolver design responsivo

	UX:
		- Desenvolva pensando e aplicando conceitos de UX (http://designculture.com.br/conceitos-fundamentais-de-um-bom-ux)
	
	Java:
		- Criar back-end utilizando Spring Boot
		- Criar camada de persistência utilizando JPA+Hibernate
		
#Hint (dicas):
	Abaixo contem algumas dicas para tornar seu projeto melhor:
		- Tente desenvolver testes unitarios
		- Tente utilizar padroes de projetos(design patterns)
		- Tente desenvolver usando o conceito single Page Application (SPA)
		- Tente usar algum framework JS, como por exemplo: Angular, Vue.js, React, AngularJS.
	  	- Tente utilizar algum framework de view, como por exemplo: bootstrap, google-material-design, metro-io
		- Tente usar Flux/Redux
		- Tente utilizar alguma ferramenta facilitadora para "unificar, minificar e obfuscar arquivos estaticos", como: webpack, gulp, grunt, etc.
		- Tente utilizar algum gerenciador de pacotes javascript(gerenciador de dependencias), como: yarn, NPM, Bower. 
		- Tente ter import de apenas um arquivo JS no arquivo HTML.				
		- Tente ter import de apenas um arquivo CSS no arquivo HTML.

Crie um fork deste repo, faça o desenvolvimento necessário e nos envie a URL.
Caso tenha problemas em criar um fork, siga o link [How to Fork in bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html#ForkingaRepository-HowtoForkaRepository)

Sugestões de IDE: [VSCODE](https://code.visualstudio.com/), [WebStorm](https://www.jetbrains.com/webstorm/), [Atom](https://atom.io/), [SublimeText](https://www.sublimetext.com/), [IntelliJ](https://www.jetbrains.com/idea/), [Eclipse](https://www.eclipse.org/), [NetBeans](https://netbeans.org/)

#Documentação (por Cassiano Moura):
    Front-end:
        - O projeto Front-end foi desenvolvido utilizando a framework Vue.js cliente + webpack com o conceito de SPA
        - A framework foi escolhida pelas dimensões do projeto, uma vez que Angular e React são melhor utilizados em sistemas de médio/grande porte
        - Foi utilizado um layout minimalista, direto e conciso, para uma experiência ágil do usuário
        - As validações de formulário foram implementadas a partir da biblioteca Vuelidate
        - Foi utilizada a framework Bootstrap no desenvolvimento do layout, com scss
        - Elementos como collapse, tabela e tab foram gerados a partir da biblioteca Element io
        - Para a formatação de datas foi utilizada a biblioteca Moment.js
        - Os gráficos foram gerados com a biblioteca c3(d3 based)
        - Os pacotes foram gerenciados por Nodejs/NPM
        - Foram implementados filtros por nome, idade e sexo na tabela de amostra de pessoas
        - Implementados gráficos de pessoas por idade e sexo (como solicitado)
    
    Back-end:
        - O projeto utiliza a liguagem java e a framework Spring Boot
        - Foi utilizado o padraõ baseado em API Rest com Controllers, Services e Repositories
        - Implementado pacote de enums para unificação de constantes
        - Implementado pacote config para configuração de CORS
        - Implementados Testes unitários utilizando Junit
        - Camada de persistência utilizando JPA + Hibernate
        - Sql de exportação do banco de dados disponível em main/resources/db.migration/crud_pessoas.sql
        - Filtros implementados utilizando Specification
        - Classe de Retorno com processamento de serviço e mensagem de feedback
