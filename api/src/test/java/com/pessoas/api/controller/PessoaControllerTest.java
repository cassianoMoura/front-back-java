package com.pessoas.api.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.pessoas.api.dto.PessoaDTO;
import com.pessoas.api.enums.TipoMensagem;
import com.pessoas.api.service.PessoaService;
import com.pessoas.api.utils.FormatarData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.util.NestedServletException;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.pessoas.api.enums.ConstantesGeral.*;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(PessoaController.class)
@ContextConfiguration
public class PessoaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private PessoaService pessoaService;

    /**
     * Testes método PessoaController.findAll()
     */
    @Test(expected = NestedServletException.class)
    public void testeBuscarTodosSemResultados() throws Exception {
        when(pessoaService.findAll(any())).thenThrow(new EntityNotFoundException(NENHUM_REGISTRO_ENCONTRADO));

        ResultActions result = mockMvc.perform(get("/pessoas").contentType("application/json"));

        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.WARN
                .getTipo()));
        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.descricao").value(
                NENHUM_REGISTRO_ENCONTRADO));
    }

    @Test
    public void testeBuscarTodosComResultados() throws Exception {
        List<PessoaDTO> pessoaDTOS = new ArrayList<>();

        for (long i = 1; i <= 10L; i++) {
            PessoaDTO pessoaDTO = criarPessoaDTOTeste();
            pessoaDTO.setId(i);
            pessoaDTOS.add(pessoaDTO);
        }

        when(pessoaService.findAll(any())).thenReturn(pessoaDTOS);
        ResultActions result = mockMvc.perform(get("/pessoas").contentType("application/json"));

        result.andExpect(status().isOk()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.SUCCESS
                .getTipo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data").value(hasSize(10)));
    }

    /**
     * Testes método PessoaController.findById()
     */
    @Test(expected = NestedServletException.class)
    public void testeBuscarPorIdSemResultado() throws Exception {
        when(pessoaService.findOne(anyLong())).thenThrow(new EntityNotFoundException(REGISTRO_NAO_ENCONTRADO));

        ResultActions result = mockMvc.perform(get("/pessoas/" + anyLong()).contentType("application/json"));

        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.WARN
                .getTipo()));
        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.descricao").value(
                NENHUM_REGISTRO_ENCONTRADO));
    }

    @Test
    public void testeBuscarPorIdComResultado() throws Exception {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();
        pessoaDTO.setId(1L);

        when(pessoaService.findOne(anyLong())).thenReturn(pessoaDTO);
        ResultActions result = mockMvc.perform(get("/pessoas/" + pessoaDTO.getId()).contentType("application/json"));

        result.andExpect(status().isOk()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.SUCCESS
                .getTipo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.id").value(pessoaDTO.getId()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.endereco").value(pessoaDTO.getEndereco()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.nome").value(pessoaDTO.getNome()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.documento").value(pessoaDTO.getDocumento()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.dtNascimento")
                .value(FormatarData.format(pessoaDTO.getDtNascimento())));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.sexo").value(pessoaDTO.getSexo().toString()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.cidade").value(pessoaDTO.getCidade()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.estado").value(pessoaDTO.getEstado()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.ativo").value(pessoaDTO.getAtivo()));
    }

    /**
     * Testes método PessoaController.save()
     */
    @Test(expected = NestedServletException.class)
    public void testeSalvarExistente() throws Exception {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();

        when(pessoaService.save(any())).thenThrow(new Error(REGISTRO_JA_EXISTENTE));

        ResultActions result = mockMvc.perform(post("/pessoas/").content(mapper.writeValueAsString
                (pessoaDTO)).contentType("application/json"));

        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.WARN
                .getTipo()));
        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.descricao").value(
                REGISTRO_JA_EXISTENTE));
    }

    @Test
    public void testeSalvarComSucesso() throws Exception {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();

        PessoaDTO pessoaDTOSaida = criarPessoaDTOTeste();
        pessoaDTOSaida.setId(1L);

        when(pessoaService.save(any())).thenReturn(pessoaDTOSaida);
        ResultActions result = mockMvc.perform(post("/pessoas/").content(mapper.writeValueAsString
                (pessoaDTO)).contentType("application/json"));

        result.andExpect(status().isOk()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.SUCCESS
                .getTipo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.id").value(pessoaDTOSaida.getId()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.endereco").value(pessoaDTOSaida.getEndereco()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.nome").value(pessoaDTOSaida.getNome()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.documento").value(pessoaDTOSaida.getDocumento()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.dtNascimento")
                .value(FormatarData.format(pessoaDTOSaida.getDtNascimento())));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.sexo").value(pessoaDTOSaida.getSexo().toString()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.ativo").value(pessoaDTOSaida.getAtivo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.estado").value(pessoaDTOSaida.getEstado()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.cidade").value(pessoaDTOSaida.getCidade()));
    }

    /**
     * Testes método PessoaController.update()
     */
    @Test(expected = NestedServletException.class)
    public void testeAtualizarNaoExistente() throws Exception {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();
        pessoaDTO.setId(1L);

        when(pessoaService.update(any())).thenThrow(new EntityNotFoundException(REGISTRO_NAO_ENCONTRADO));

        ResultActions result = mockMvc.perform(post("/pessoas/" + pessoaDTO.getId()).content(mapper.writeValueAsString
                (pessoaDTO)).contentType("application/json"));

        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.WARN
                .getTipo()));
        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.descricao").value(
                REGISTRO_NAO_ENCONTRADO));
    }

    @Test
    public void testeAtualizarComSucesso() throws Exception {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();

        PessoaDTO pessoaDTOSaida = criarPessoaDTOTeste();
        pessoaDTOSaida.setId(1L);
        pessoaDTOSaida.setDocumento("Teste update");
        pessoaDTOSaida.setNome("Teste updated");
        pessoaDTOSaida.setEndereco("Endereco teste update");
        pessoaDTOSaida.setDocumento("00121");
        pessoaDTOSaida.setSexo('F');
        pessoaDTOSaida.setDtNascimento(LocalDateTime.of(2018, 2, 22, 0, 0, 0));

        when(pessoaService.update(any())).thenReturn(pessoaDTOSaida);
        ResultActions result = mockMvc.perform(post("/pessoas/" + 1L).content(mapper.writeValueAsString
                (pessoaDTO)).contentType("application/json"));

        result.andExpect(status().isOk()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.SUCCESS
                .getTipo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.id").value(pessoaDTOSaida.getId()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.endereco").value(pessoaDTOSaida.getEndereco()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.nome").value(pessoaDTOSaida.getNome()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.documento").value(pessoaDTOSaida.getDocumento()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.dtNascimento")
                .value(FormatarData.format(pessoaDTOSaida.getDtNascimento())));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.sexo").value(pessoaDTOSaida.getSexo().toString()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.ativo").value(pessoaDTOSaida.getAtivo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.estado").value(pessoaDTOSaida.getEstado()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.cidade").value(pessoaDTOSaida.getCidade()));
    }

    /**
     * Testes método PessoaController.disable()
     */
    @Test(expected = NestedServletException.class)
    public void testeDesativarNaoExistente() throws Exception {
        when(pessoaService.disable(any())).thenThrow(new EntityNotFoundException(REGISTRO_NAO_ENCONTRADO));

        ResultActions result = mockMvc.perform(post("/pessoas/desativar/" + 1L)
                .contentType("application/json"));

        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.WARN
                .getTipo()));
        result.andExpect(status().isNotFound()).andExpect(jsonPath("$.mensagem.descricao").value(
                REGISTRO_NAO_ENCONTRADO));
    }

    @Test
    public void testeDesativarComSucesso() throws Exception {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();
        pessoaDTO.setId(1L);
        pessoaDTO.setAtivo(STATUS_INATIVO);

        when(pessoaService.disable(anyLong())).thenReturn(pessoaDTO);
        ResultActions result = mockMvc.perform(post("/pessoas/desativar/" + pessoaDTO.getId())
                .content(mapper.writeValueAsString(pessoaDTO)).contentType("application/json"));

        result.andExpect(status().isOk()).andExpect(jsonPath("$.mensagem.tipo").value(TipoMensagem.SUCCESS
                .getTipo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.id").value(pessoaDTO.getId()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.endereco").value(pessoaDTO.getEndereco()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.nome").value(pessoaDTO.getNome()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.documento").value(pessoaDTO.getDocumento()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.dtNascimento")
                .value(FormatarData.format(pessoaDTO.getDtNascimento())));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.sexo").value(pessoaDTO.getSexo().toString()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.ativo").value(pessoaDTO.getAtivo()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.estado").value(pessoaDTO.getEstado()));
        result.andExpect(status().isOk()).andExpect(jsonPath("$.data.cidade").value(pessoaDTO.getCidade()));
    }

    public PessoaDTO criarPessoaDTOTeste() {
        PessoaDTO pessoaDTO = new PessoaDTO();
        pessoaDTO.setNome("Teste");
        pessoaDTO.setEndereco("Endereco teste");
        pessoaDTO.setDocumento("001");
        pessoaDTO.setSexo('M');
        pessoaDTO.setDtNascimento(LocalDateTime.of(2018, 1, 1, 0, 0, 0));
        pessoaDTO.setAtivo(STATUS_ATIVO);
        pessoaDTO.setCidade("Cidade teste");
        pessoaDTO.setEstado("Estado teste");
        return pessoaDTO;
    }

}
