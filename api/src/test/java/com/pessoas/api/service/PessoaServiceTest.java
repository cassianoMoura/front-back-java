package com.pessoas.api.service;

import com.pessoas.api.dto.PessoaDTO;
import com.pessoas.api.entity.Pessoa;
import com.pessoas.api.repository.PessoaRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.pessoas.api.enums.ConstantesGeral.STATUS_ATIVO;
import static com.pessoas.api.enums.ConstantesGeral.STATUS_INATIVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("test")
public class PessoaServiceTest {
    @Mock
    private PessoaRepository pessoaRepository;

    private PessoaService pessoaService;

    @Before
    public void setup(){
        pessoaService = new PessoaService(pessoaRepository);
    }

    /**
     * Testes PessoaService.update()
     */
    @Test(expected = EntityNotFoundException.class)
    public void deveraDarExcecaoAoFazerUpdateComEntidadeInexistente() {

        PessoaDTO borderoDTO = criarPessoaDTOTeste();
        borderoDTO.setId(10L);
        when(pessoaRepository.findById(anyLong())).thenReturn(Optional.empty());

        pessoaService.update(borderoDTO);
    }

    @Test
    public void deveraEfetuarOUpdateComSucesso() {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();
        pessoaDTO.setId(1L);

        Pessoa pessoa = criarPessoaTeste();
        pessoa.setId(1L);
        
        when(pessoaRepository.findById(anyLong()))
                .thenReturn(Optional.of(pessoa));
        when(pessoaRepository.save(any(Pessoa.class)))
                .thenReturn(pessoa);
        PessoaDTO pessoaDTOSaida = pessoaService.update(pessoaDTO);

        assertThat(pessoaDTOSaida.getId()).isEqualTo(pessoaDTO.getId());
        assertThat(pessoaDTOSaida.getAtivo()).isEqualTo(pessoaDTO.getAtivo());
        assertThat(pessoaDTOSaida.getCidade()).isEqualTo(pessoaDTO.getCidade());
        assertThat(pessoaDTOSaida.getDocumento()).isEqualTo(pessoaDTO.getDocumento());
        assertThat(pessoaDTOSaida.getDtNascimento()).isEqualTo(pessoaDTO.getDtNascimento());
        assertThat(pessoaDTOSaida.getEndereco()).isEqualTo(pessoaDTO.getEndereco());
        assertThat(pessoaDTOSaida.getEstado()).isEqualTo(pessoaDTO.getEstado());
        assertThat(pessoaDTOSaida.getNome()).isEqualTo(pessoaDTO.getNome());
        assertThat(pessoaDTOSaida.getSexo()).isEqualTo(pessoaDTO.getSexo());

    }

    /**
     *  Testes PessoaService.findOne()
     */
    @Test(expected = EntityNotFoundException.class)
    public void deveraDarExcessaoAoProcurarEntidadeInexistente() {
        when(pessoaRepository.findById(anyLong())).thenReturn(Optional.empty());
        pessoaService.findOne(1L);
    }

    @Test
    public void deveraRetornarUmaEntidadeAoPesquisarPorIdExistente() {
        Pessoa pessoa = criarPessoaTeste();
        pessoa.setId(1L);
        when(pessoaRepository.findById(anyLong())).thenReturn(Optional.of(pessoa));
        PessoaDTO pessoaDTO = pessoaService.findOne(1L);

        assertThat(pessoa.getId()).isEqualTo(pessoaDTO.getId());
        assertThat(pessoa.getAtivo()).isEqualTo(pessoaDTO.getAtivo());
        assertThat(pessoa.getCidade()).isEqualTo(pessoaDTO.getCidade());
        assertThat(pessoa.getDocumento()).isEqualTo(pessoaDTO.getDocumento());
        assertThat(pessoa.getDtNascimento()).isEqualTo(pessoaDTO.getDtNascimento());
        assertThat(pessoa.getEndereco()).isEqualTo(pessoaDTO.getEndereco());
        assertThat(pessoa.getEstado()).isEqualTo(pessoaDTO.getEstado());
        assertThat(pessoa.getNome()).isEqualTo(pessoaDTO.getNome());
        assertThat(pessoa.getSexo()).isEqualTo(pessoaDTO.getSexo());
    }

    /**
     * Testes PessoaService.save()
     */
    @Test(expected = Error.class)
    public void deveraDarExcessaoAoTentarSalvarPessoaExistente() {
        Pessoa pessoa = criarPessoaTeste();
        pessoa.setId(1L);
        when(pessoaRepository.findById(anyLong())).thenReturn(Optional.of(pessoa));
        pessoaService.save(new PessoaDTO(pessoa));
    }

    @Test
    public void deveraSalvarComSucesso() {
        PessoaDTO pessoaDTOEntrada = criarPessoaDTOTeste();
        Pessoa pessoa = criarPessoaTeste();

        when(pessoaRepository.save(any(Pessoa.class))).thenReturn(pessoa);

        PessoaDTO pessoaDTOSaida = pessoaService.save(pessoaDTOEntrada);

        assertThat(pessoaDTOSaida.getId()).isEqualTo(pessoaDTOEntrada.getId());
        assertThat(pessoaDTOSaida.getAtivo()).isEqualTo(pessoaDTOEntrada.getAtivo());
        assertThat(pessoaDTOSaida.getCidade()).isEqualTo(pessoaDTOEntrada.getCidade());
        assertThat(pessoaDTOSaida.getDocumento()).isEqualTo(pessoaDTOEntrada.getDocumento());
        assertThat(pessoaDTOSaida.getDtNascimento()).isEqualTo(pessoaDTOEntrada.getDtNascimento());
        assertThat(pessoaDTOSaida.getEndereco()).isEqualTo(pessoaDTOEntrada.getEndereco());
        assertThat(pessoaDTOSaida.getEstado()).isEqualTo(pessoaDTOEntrada.getEstado());
        assertThat(pessoaDTOSaida.getNome()).isEqualTo(pessoaDTOEntrada.getNome());
        assertThat(pessoaDTOSaida.getSexo()).isEqualTo(pessoaDTOEntrada.getSexo());
    }

    /**
     * Testes PessoaService.findAll()
     */

    @Test
    public void deveraRetornarUmaListaDeBorderos() {
        List<Pessoa> pessoas = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            pessoas.add(new Pessoa());
        }
        when(pessoaRepository.findAll(any())).thenReturn(pessoas);

        List<PessoaDTO> retorno = pessoaService.findAll(any());

        assertThat(retorno.size()).isEqualTo(3);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deveraRetornarUmaListaVaziaSeNenhumRegistroExistir() {
        when(pessoaRepository.findAll(any())).thenReturn(Collections.emptyList());

        pessoaService.findAll(any());
    }

    /**
     * Testes PessoaService.disable()
     */
    @Test(expected = EntityNotFoundException.class)
    public void deveraDarExcecaoAoDesativarEntidadeInexistente() {
        when(pessoaRepository.findById(anyLong())).thenReturn(Optional.empty());

        pessoaService.disable(1L);
    }

    @Test
    public void deveraDesativarComSucesso() {
        PessoaDTO pessoaDTO = criarPessoaDTOTeste();
        pessoaDTO.setId(1L);
        pessoaDTO.setAtivo(STATUS_INATIVO);

        Pessoa pessoa = criarPessoaTeste();
        pessoa.setId(1L);

        Pessoa pessoaDesativada = criarPessoaTeste();
        pessoaDesativada.setId(1L);
        pessoaDesativada.setAtivo(STATUS_INATIVO);

        when(pessoaRepository.findById(anyLong()))
                .thenReturn(Optional.of(pessoa));
        when(pessoaRepository.save(any(Pessoa.class)))
                .thenReturn(pessoaDesativada);
        PessoaDTO pessoaDTOSaida = pessoaService.update(pessoaDTO);

        assertThat(pessoaDTOSaida.getId()).isEqualTo(pessoaDTO.getId());
        assertThat(pessoaDTOSaida.getAtivo()).isEqualTo(pessoaDTO.getAtivo());
        assertThat(pessoaDTOSaida.getCidade()).isEqualTo(pessoaDTO.getCidade());
        assertThat(pessoaDTOSaida.getDocumento()).isEqualTo(pessoaDTO.getDocumento());
        assertThat(pessoaDTOSaida.getDtNascimento()).isEqualTo(pessoaDTO.getDtNascimento());
        assertThat(pessoaDTOSaida.getEndereco()).isEqualTo(pessoaDTO.getEndereco());
        assertThat(pessoaDTOSaida.getEstado()).isEqualTo(pessoaDTO.getEstado());
        assertThat(pessoaDTOSaida.getNome()).isEqualTo(pessoaDTO.getNome());
        assertThat(pessoaDTOSaida.getSexo()).isEqualTo(pessoaDTO.getSexo());
    }

    public Pessoa criarPessoaTeste() {
        return new Pessoa(criarPessoaDTOTeste());
    }

    public PessoaDTO criarPessoaDTOTeste() {
        PessoaDTO pessoaDTO = new PessoaDTO();
        pessoaDTO.setNome("Teste");
        pessoaDTO.setEndereco("Endereco teste");
        pessoaDTO.setDocumento("001");
        pessoaDTO.setSexo('M');
        pessoaDTO.setDtNascimento(LocalDateTime.of(2018, 1, 1, 0, 0, 0));
        pessoaDTO.setAtivo(STATUS_ATIVO);
        pessoaDTO.setCidade("Cidade teste");
        pessoaDTO.setEstado("Estado teste");
        return pessoaDTO;
    }
}
