package com.pessoas.api.service;

import com.pessoas.api.dto.PessoaDTO;
import com.pessoas.api.entity.Pessoa;
import com.pessoas.api.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.pessoas.api.enums.ConstantesGeral.*;

/**
 * Created by Cassiano Moura on 10/08/18.
 */

@Service
public class PessoaService {

    private PessoaRepository pessoaRepository;

    @Autowired
    public PessoaService(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    public List<PessoaDTO> findAll(Specification filtros) {
        List<Pessoa> pessoas = pessoaRepository.findAll(filtros);
        if (pessoas.isEmpty()) {
            throw new EntityNotFoundException(NENHUM_REGISTRO_ENCONTRADO);
        }
        return pessoas.stream()
                .map(PessoaDTO::new)
                .collect(Collectors.toList());
    }

    public PessoaDTO findOne(Long id) {
        Optional<Pessoa> pessoa = pessoaRepository.findById(id);

        if (!pessoa.isPresent()) {
            throw new EntityNotFoundException(REGISTRO_NAO_ENCONTRADO);
        }
        return new PessoaDTO(pessoa.get());
    }

    public PessoaDTO save(PessoaDTO pessoaDTO) {
        if (pessoaDTO.getId() != null && pessoaRepository.findById(pessoaDTO.getId()).isPresent()) {
            throw new Error(REGISTRO_JA_EXISTENTE);
        }
        pessoaDTO = new PessoaDTO(pessoaRepository.save(new Pessoa(pessoaDTO)));

        return pessoaDTO;
    }

    public PessoaDTO update(PessoaDTO pessoaDTO) {
        Pessoa pessoa = getPessoaOrElseThrow(pessoaDTO.getId());
        Integer statusAtual = pessoa.getAtivo();
        pessoa.setAllFields(pessoaDTO);
        pessoa.setAtivo(statusAtual);
        pessoaDTO = new PessoaDTO(pessoaRepository.save(pessoa));

        return pessoaDTO;
    }

    public PessoaDTO disable(Long id) {
        Pessoa pessoa = getPessoaOrElseThrow(id);
        pessoa.setAtivo(STATUS_INATIVO);

        return new PessoaDTO(pessoaRepository.save(pessoa));
    }

    private Pessoa getPessoaOrElseThrow(Long id) {
        return pessoaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(REGISTRO_NAO_ENCONTRADO));
    }
}
