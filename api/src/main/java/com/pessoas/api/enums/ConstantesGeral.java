package com.pessoas.api.enums;

/**
 * Created by Cassiano Moura on 10/08/18.
 */

public class ConstantesGeral {

    /**
     * Constantes de status de pessoas
     */
    public static final int STATUS_ATIVO = 1;
    public static final int STATUS_INATIVO = 2;

    /**
     * Constantes de mensagens de feedback
     */
    public static final String NENHUM_REGISTRO_ENCONTRADO = "Nenhum registro encontrado.";
    public static final String REGISTRO_NAO_ENCONTRADO = "Registro não encontrado.";
    public static final String REGISTRO_JA_EXISTENTE = "Registro já existente.";
    public static final String SUCESSO = "Sucesso.";
}
