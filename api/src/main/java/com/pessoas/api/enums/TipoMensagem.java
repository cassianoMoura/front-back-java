package com.pessoas.api.enums;

/**
 * Created by Cassiano Moura on 10/08/18.
 */
public enum TipoMensagem {
    INFO("info"),
    WARN("warning"),
    ERROR("error"),
    SUCCESS("success");

    private String tipo;

    TipoMensagem(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
}
