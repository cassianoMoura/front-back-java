package com.pessoas.api.repository;

import com.pessoas.api.entity.Pessoa;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Cassiano Moura on 10/08/18.
 */

@Repository
public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

    List<Pessoa> findAll(Specification filtros);

}
