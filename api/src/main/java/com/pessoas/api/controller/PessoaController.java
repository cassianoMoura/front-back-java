package com.pessoas.api.controller;

import com.pessoas.api.dto.PessoaDTO;
import com.pessoas.api.filter.PessoaCriaFiltros;
import com.pessoas.api.filter.PessoaFiltros;
import com.pessoas.api.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.pessoas.api.entity.Retorno.processaRetornoServico;

/**
 * Created by Cassiano Moura on 10/08/18.
 */

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    private PessoaService pessoaService;

    @Autowired
    public PessoaController(PessoaService pessoaService) {
        this.pessoaService = pessoaService;
    }

    @GetMapping
    public ResponseEntity findAll(@RequestParam(required = false) Integer status) {
        PessoaFiltros filtros = new PessoaCriaFiltros()
                .filtraAtivo(status)
                .build();

        return processaRetornoServico(() -> pessoaService.findAll(filtros), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        return processaRetornoServico(() -> pessoaService.findOne(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity save(@RequestBody @Valid PessoaDTO pessoaDTO) {
        return processaRetornoServico(() -> pessoaService.save(pessoaDTO), HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity update(@PathVariable Long id,
                                 @RequestBody @Valid PessoaDTO pessoaDTO) {
        pessoaDTO.setId(id);
        return processaRetornoServico(() -> pessoaService.update(pessoaDTO), HttpStatus.OK);
    }

    @PostMapping("/desativar/{id}")
    public ResponseEntity disable(@PathVariable Long id) {
        return processaRetornoServico(() -> pessoaService.disable(id), HttpStatus.OK);
    }
}
