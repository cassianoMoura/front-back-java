package com.pessoas.api.filter;

public class PessoaCriaFiltros {

    private Integer ativo;

    public PessoaFiltros build() {
        return new PessoaFiltros(this);
    }

    public PessoaCriaFiltros filtraAtivo(Integer ativo) {
        this.ativo = ativo;
        return this;
    }

    public Integer getAtivo() {
        return ativo;
    }
}
