package com.pessoas.api.filter;

import com.pessoas.api.entity.Pessoa;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PessoaFiltros implements Specification<Pessoa> {

    private Optional<Integer> ativo;

    public PessoaFiltros() {
    }

    protected PessoaFiltros(PessoaCriaFiltros pessoaCriaFiltros) {
        this.ativo = Optional.ofNullable(pessoaCriaFiltros.getAtivo());
    }

    public Optional<Integer> getAtivo() {
        return ativo;
    }

    @Override
    public Predicate toPredicate(Root<Pessoa> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        List<Predicate> predicates = new ArrayList<>();
        ativo.ifPresent(n -> predicates.add(criteriaBuilder.equal(root.get("ativo"), n)));

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
