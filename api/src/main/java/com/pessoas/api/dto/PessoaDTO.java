package com.pessoas.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pessoas.api.entity.Pessoa;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

/**
 * Created by Cassiano Moura on 10/08/18.
 */

public class PessoaDTO {

    private Long id;

    private String nome;

    private String endereco;

    private String cidade;

    private String estado;

    private String documento;

    @ApiModelProperty(example = "2017-01-01 01:01:01")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dtNascimento;

    private Character sexo;

    private Integer ativo;

    public PessoaDTO() {
    }

    public PessoaDTO(Long id) {
        this.id = id;
    }

    public PessoaDTO(Pessoa pessoa) {
        setId(pessoa.getId());
        setNome(pessoa.getNome());
        setEndereco(pessoa.getEndereco());
        setDocumento(pessoa.getDocumento());
        setDtNascimento(pessoa.getDtNascimento());
        setSexo(pessoa.getSexo());
        setAtivo(pessoa.getAtivo());
        setCidade(pessoa.getCidade());
        setEstado(pessoa.getEstado());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public LocalDateTime getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(LocalDateTime dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public Integer getAtivo() {
        return ativo;
    }

    public void setAtivo(Integer ativo) {
        this.ativo = ativo;
    }
}
