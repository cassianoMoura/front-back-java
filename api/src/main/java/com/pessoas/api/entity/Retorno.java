package com.pessoas.api.entity;

import com.pessoas.api.enums.TipoMensagem;
import com.pessoas.api.utils.RetornoMensagem;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.function.Supplier;

/**
 * Created by Cassiano Moura on 10/08/18.
 */

public class Retorno {

    private Object data;

    private RetornoMensagem mensagem;

    public Retorno() {
        this.setData(null);
        this.setMensagem(null);
    }

    public static <T> ResponseEntity<Retorno> processaRetornoServico(Supplier<T> supplier, HttpStatus httpStatus) {
        return processaRetornoServico(supplier, httpStatus, RetornoMensagem.MSG_SUCESSO);
    }


    public static ResponseEntity<Retorno> processaRetornoServico(HttpStatus httpStatus, TipoMensagem tipoMensagem, String message) {
        return processaRetornoServico(() -> null, httpStatus, new RetornoMensagem(tipoMensagem.getTipo(), message));
    }

    private static <T> ResponseEntity<Retorno> processaRetornoServico(Supplier<T> supplier, HttpStatus httpStatus,
                                                                      RetornoMensagem retornoMensagem) {
        Retorno retorno = new Retorno();
        retorno.setData(supplier.get());
        retorno.setMensagem(retornoMensagem);
        return ResponseEntity.status(httpStatus).body(retorno);
    }


    public RetornoMensagem getMensagem() {
        return mensagem;
    }

    public void setMensagem(RetornoMensagem mensagem) {
        this.mensagem = mensagem;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
