package com.pessoas.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pessoas.api.dto.PessoaDTO;
import com.pessoas.api.utils.UniqueIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.pessoas.api.enums.ConstantesGeral.STATUS_ATIVO;

/**
 * Created by Cassiano Moura on 10/08/18.
 */

@Entity
@Table(name = "tb_pessoas")
public class Pessoa {

    @Id
    @Column(updatable = false)
    private Long id;

    @Length(max = 255)
    @Column
    private String nome;

    @Length(max = 255)
    @Column
    private String endereco;

    @Length(max = 255)
    @Column
    private String cidade;

    @Length(max = 255)
    @Column
    private String estado;

    @Length(max = 255)
    @Column
    private String documento;

    @Column
    private Character sexo;

    @ApiModelProperty(example = "2017-01-01 01:01:01")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dtNascimento;

    @Column
    private Integer ativo;

    public Pessoa() {
    }

    public Pessoa(Long id) {
        this.id = id;
    }

    public Pessoa(PessoaDTO pessoaDTO) {
        setAllFields(pessoaDTO);
    }

    public void setAllFields(PessoaDTO pessoaDTO) {
        setId(pessoaDTO.getId());
        setNome(pessoaDTO.getNome());
        setEndereco(pessoaDTO.getEndereco());
        setDocumento(pessoaDTO.getDocumento());
        setDtNascimento(pessoaDTO.getDtNascimento());
        setSexo(pessoaDTO.getSexo());
        setAtivo(pessoaDTO.getAtivo() != null ? pessoaDTO.getAtivo() : STATUS_ATIVO);
        setCidade(pessoaDTO.getCidade());
        setEstado(pessoaDTO.getEstado());
    }

    @PrePersist
    private void prePersist() {
        if (id == null) {
            id = UniqueIdGenerator.next();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public LocalDateTime getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(LocalDateTime dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public Integer getAtivo() {
        return ativo;
    }

    public void setAtivo(Integer ativo) {
        this.ativo = ativo;
    }
}
