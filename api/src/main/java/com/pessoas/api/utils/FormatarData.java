package com.pessoas.api.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class FormatarData {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private FormatarData() {
        throw new IllegalStateException("Utility class");
    }

    public static String format(LocalDateTime data) {
        return data.format(formatter);
    }

    public static String formatNow() {
        return LocalDateTime.now().format(formatter);
    }
}
