package com.pessoas.api.utils;

import com.pessoas.api.enums.TipoMensagem;

import static com.pessoas.api.enums.ConstantesGeral.SUCESSO;

/**
 * Created by Cassiano Moura on 10/08/18.
 */
public class RetornoMensagem {

    public static final RetornoMensagem MSG_SUCESSO = new RetornoMensagem(TipoMensagem.SUCCESS.getTipo(), SUCESSO);

    private String tipo;
    private String descricao;

    public RetornoMensagem() {
    }

    public RetornoMensagem(String tipo, String descricao) {
        this.setTipo(tipo);
        this.setDescricao(descricao);
    }

    public String getTipo() {
        return tipo;
    }

    private void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    private void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
