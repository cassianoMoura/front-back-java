package com.pessoas.api.utils;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Cassiano Moura on 10/08/18.
 */
public final class UniqueIdGenerator {
    private static final int TAMANHO_BITS = 8;
    private static final int DIVISOR = 256;
    private static AtomicLong counter = new AtomicLong();

    private UniqueIdGenerator() {
        // $COVERAGE-IGNORE$
    }

    // Gera até 1024 identificadores únicos por hora.
    public static long next() {
        long id = System.currentTimeMillis() << TAMANHO_BITS;
        id |= counter.getAndIncrement() % DIVISOR;
        return id;
    }
}
